require.config({
    paths: {
        backbone: '../node_modules/backbone/backbone',
        jquery: '../node_modules/jquery/dist/jquery',
        localstorage: '../node_modules/backbone.localstorage/backbone.localStorage',
        text: '../node_modules/requirejs-text/text',
        underscore: '../node_modules/backbone/node_modules/underscore/underscore',
    }
});

require(['app'], function (App) {
    'use strict';

    $(function () {
        new App();
    });
});
