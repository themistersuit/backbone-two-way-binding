define(['backbone', 'localstorage'], function (Backbone) {
    'use strict';

    var Model = Backbone.Model.extend({
        localStorage: new Backbone.LocalStorage('User'),

        defaults: {
            first_name: 'Yehuda'
        },

        parse: function(response) {
            return response[0];
        },

        validate: function(attrs) {
            var errors = [];
            return errors.length ? errors : false;
        }
    });

    return Model;
});
