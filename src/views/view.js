define(['backbone', 'text!/templates/view.html'], function (Backbone, html) {
    'use strict';

    var View = Backbone.View.extend({
        template: _.template(html),

        events: {
            'input input': 'keyup'
        },

        initialize: function() {
            this.render();

            this.listenTo(this.model, 'invalid', this.showErrors);
            this.listenTo(this.model, 'change:first_name', this.renderFirstName);
        },

        render: function() {
            this.el.innerHTML = this.template(this.model.attributes);
            return this;
        },

        renderFirstName: function(model) {
            this.$('h2').text(model.get('first_name'));
        },

        keyup: function(e) {
            $(e.target).closest('.form-control')
                .removeClass('error')
                .find('.error-label').text('');

            this.model.save('first_name', e.target.value);
        },

        showErrors: function(model, errors) {
            _.each(errors, function (error) {
                this.$('.form-control')
                    .addClass('error')
                    .find('.error-label').text(error.message);
            }, this);
        }
    });

    return View;
});
