define([
    'backbone',
    'models/model',
    'views/view'
], function (Backbone, Model, View) {
    'use strict';

    var App = Backbone.View.extend({
        el: 'main',

        initialize: function() {
            var self = this;

            this.model = new Model();

            this.model.fetch({
                success: function(model) {
                    self.$el.append(
                        new View({ model: model }).el
                    );
                }
            });
        }
    });

    return App;
});
